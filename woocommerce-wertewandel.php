<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://christophrado.de
 * @since             1.0.0
 * @package           Woocommerce_Wertewandel
 *
 * @wordpress-plugin
 * Plugin Name:       WooCommerce Wertewandel Integration
 * Plugin URI:        https://christophrado.de/wertewandel
 * Description:       Integration of Wertewandel into WooCommerce allowing customers to automatically collect points for placed orders.
 * Version:           0.9.0
 * Author:            Christoph Rado
 * Author URI:        https://christophrado.de
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       woocommerce-wertewandel
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently pligin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'WOOCOMMERCE_WERTEWANDEL_VERSION', '0.9.0' );

if( 'no' == get_option( 'wcww_settings_use_sandbox_mode', 'no' ) ) {

	define( 'API_HOST', 'https://api.wertewandel.de/' );

} else {

	define( 'API_HOST', 'https://api-sandbox.wertewandel.de/' );

}

define( 'API_VERSION', 'v1' );



/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-woocommerce-wertewandel-activator.php
 */
function activate_woocommerce_wertewandel() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-woocommerce-wertewandel-activator.php';
	Woocommerce_Wertewandel_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-woocommerce-wertewandel-deactivator.php
 */
function deactivate_woocommerce_wertewandel() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-woocommerce-wertewandel-deactivator.php';
	Woocommerce_Wertewandel_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_woocommerce_wertewandel' );
register_deactivation_hook( __FILE__, 'deactivate_woocommerce_wertewandel' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-woocommerce-wertewandel.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_woocommerce_wertewandel() {

	$plugin = new Woocommerce_Wertewandel();
	$plugin->run();

}
run_woocommerce_wertewandel();

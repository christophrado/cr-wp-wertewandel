��          �      ,      �     �  
   �  ^   �     �  J        V  
   j     u     �     �  <   �  8   �  /        N     Z  W   v  �  �     w  
   ~  P   �     �  U   �     I  
   ^     i     �     �  G   �  >   �  )   .     X     d  c   �                                                          
      	              App-ID Auth-Token Enter your Wertewandel API integration credentials you received during the onboarding process. General Settings Not a Wertewandel user yet? <a href="%s" target="_blank">Register now!</a> Partner Credentials Partner-ID Regular Input Field Sandbox Mode Show Registration Show registration info below account input field on checkout Use a regular input field instead of a full-sized banner Use the sandbox API (for testing purposes only) Wertewandel Wertewandel Customer Number Your customer number could not be confirmed with Wertewandel. Please verify your input. Project-Id-Version: WooCommerce Wertewandel
POT-Creation-Date: 2017-12-11 22:01+0100
PO-Revision-Date: 2017-12-12 11:12+0100
Last-Translator: 
Language-Team: Christoph Rado
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.5
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: .
 App-ID Auth-Token Gib deine Wertewandel API-Daten ein, die du im Onboarding-Prozess erhalten hast. Allgemeine Einstellungen Noch ein Wertewandel-Mitglied? <a href="%s" target="_blank">Registere dich jetzt!</a> Partner-Zugangsdaten Partner-ID Reguläres Eingabefeld Sandbox-Modus Zeige Registrier-Hinweis Zeige Registrier-Hinweis unter dem Eingabefeld für die Mitgliedsnummer Nutze ein normales Texteingabefeld statt eines großen Banners Sandbox-API benutzen (nur zu Testzwecken) Wertewandel Wertewandel Mitgliedsnummer Deine Mitgliedsnummer konnte nicht durch Wertewandel bestätigt werden. Bitte prüfe deine Eingabe. 
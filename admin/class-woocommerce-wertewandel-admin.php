<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://christophrado.de
 * @since      1.0.0
 *
 * @package    Woocommerce_Wertewandel
 * @subpackage Woocommerce_Wertewandel/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Woocommerce_Wertewandel
 * @subpackage Woocommerce_Wertewandel/admin
 * @author     Christoph Rado <wordpress@christophrado.de>
 */
class Woocommerce_Wertewandel_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		add_filter( 'woocommerce_settings_tabs_array', __CLASS__ . '::add_settings_tab', 50 );
		add_action( 'woocommerce_settings_tabs_wertewandel', __CLASS__ . '::settings_tab' );
		add_action( 'woocommerce_update_options_wertewandel', __CLASS__ . '::update_settings' );

		self::enqueue_scripts();


		// Hooks near the bottom of profile page (if current user) 
		add_action('show_user_profile', __CLASS__ . '::custom_user_profile_fields');
		
		// Hooks near the bottom of the profile page (if not current user) 
		add_action('edit_user_profile', __CLASS__ . '::custom_user_profile_fields');
		
		// @param WP_User $user
		
		
		// Hook is used to save custom fields that have been added to the WordPress profile page (if current user) 
		add_action( 'personal_options_update', __CLASS__ . '::update_extra_profile_fields' );
		
		// Hook is used to save custom fields that have been added to the WordPress profile page (if not current user) 
		add_action( 'edit_user_profile_update', __CLASS__ . '::update_extra_profile_fields' );
		

	}

		public static function custom_user_profile_fields( $user ) {
		?>
		    <table class="form-table">
		        <tr>
		            <th>
		                <label for="wertewandel-customer-no"><?php _e( 'Wertewandel Customer Number' ); ?></label>
		            </th>
		            <td>
		                <input type="text" name="wertewandel-customer-no" id="wertewandel-customer-no" value="<?php echo esc_attr( get_the_author_meta( 'wertewandel-customer-no', $user->ID ) ); ?>" class="regular-text" />
		            </td>
		        </tr>
		    </table>
		<?php
		}

		public static function update_extra_profile_fields( $user_id ) {
		    if ( current_user_can( 'edit_user', $user_id ) )
		        update_user_meta( $user_id, 'wertewandel-customer-no', $_POST['wertewandel-customer-no'] );
		}




	/**
	 * Add a new settings tab to the WooCommerce settings tabs array.
	 *
	 * @param array $settings_tabs Array of WooCommerce setting tabs & their labels, excluding the Subscription tab.
	 * @return array $settings_tabs Array of WooCommerce setting tabs & their labels, including the Subscription tab.
	 */
	public static function add_settings_tab( $settings_tabs ) {
		$settings_tabs['wertewandel'] = __( 'Wertewandel', 'woocommerce-wertewandel' );
		return $settings_tabs;
	}

	/**
	 * Uses the WooCommerce admin fields API to output settings via the @see woocommerce_admin_fields() function.
	 *
	 * @uses woocommerce_admin_fields()
	 * @uses self::get_settings()
	 */
	public static function settings_tab() {
		woocommerce_admin_fields( self::get_settings() );
	}

	/**
	 * Uses the WooCommerce options API to save settings via the @see woocommerce_update_options() function.
	 *
	 * @uses woocommerce_update_options()
	 * @uses self::get_settings()
	 */
	public static function update_settings() {
		// ADD API CHECK FOR APP ID / AUTH TOKEN
		// DISABLE PLUGIN IF NOT VALID
		woocommerce_update_options( self::get_settings() );
	}

	/**
	 * Get all the settings for this plugin for @see woocommerce_admin_fields() function.
	 *
	 * @return array Array of settings for @see woocommerce_admin_fields() function.
	 */
	public static function get_settings() {
		$settings = array(
			'general_section_start' => array(
				'name'		=> __( 'General Settings', 'woocommerce-wertewandel' ),
				'type'		=> 'title',
				'id'		=> 'wcww_settings_general_section_start'
			),
/*
			'enable_plugin'	=> array(
				'name'		=> __( 'Enable Plugin', 'woocommerce-wertewandel' ),
				'desc'		=> __( 'Enable Wertewandel API integration', 'woocommerce-wertewandel' ),
				'type'		=> 'checkbox',
				'id'		=> 'wcww_settings_enable_plugin'
			),
*/
			'sandbox_mode' => array(
				'name'		=> __( 'Sandbox Mode', 'woocommerce-wertewandel' ),
				'desc'		=> __( 'Use the sandbox API (for testing purposes only)', 'woocommerce-wertewandel' ),
				'id'		=> 'wcww_settings_use_sandbox_mode',
				'type'		=> 'checkbox',
				'default'	=> 'no',
			),
			'use_native_input' => array(
				'name'		=> __( 'Regular Input Field', 'woocommerce-wertewandel' ),
				'desc'		=> __( 'Use a regular input field instead of a full-sized banner', 'woocommerce-wertewandel' ),
				'id'		=> 'wcww_settings_use_native_input',
				'type'		=> 'checkbox',
				'default'	=> 'no',
			),
			'show_registration' => array(
				'name'		=> __( 'Show Registration', 'woocommerce-wertewandel' ),
				'desc'		=> __( 'Show registration info below account input field on checkout', 'woocommerce-wertewandel' ),
				'id'		=> 'wcww_settings_show_registration_info',
				'type'		=> 'checkbox',
				'default'	=> 'yes',
			),
			'general_section_end' => array(
				'type'		=> 'sectionend',
				'id'		=> 'wcww_settings_general_section_end',
			),
			'api_section_start' => array(
				'name'		=> __( 'Partner Credentials', 'woocommerce-wertewandel' ),
				'desc'		=> __( 'Enter your Wertewandel API integration credentials you received during the onboarding process.', 'woocommerce-wertewandel' ),
				'id'		=> 'wcww_settings_api_section_start',
				'type'		=> 'title',
			),
			'partner_id' => array(
				'name'		=> __( 'Partner-ID', 'woocommerce-wertewandel' ),
				'id'		=> 'wcww_settings_partnerid',
				'type'		=> 'text',
			),
			'app_id' => array(
				'name'		=> __( 'App-ID', 'woocommerce-wertewandel' ),
				'id'		=> 'wcww_settings_appid',
				'type'		=> 'text',
			),
			'auth_token' => array(
				'name'		=> __( 'Auth-Token', 'woocommerce-wertewandel' ),
				'id'		=> 'wcww_settings_authtoken',
				'type'		=> 'text',
			),
			'api_section_end' => array(
				'type'		=> 'sectionend',
				'id'		=> 'wcww_settings_api_section_end',
			),
		);
		return apply_filters( 'settings_tab_wertewandel', $settings );
	}



	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Woocommerce_Wertewandel_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Woocommerce_Wertewandel_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/woocommerce-wertewandel-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Woocommerce_Wertewandel_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Woocommerce_Wertewandel_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/woocommerce-wertewandel-admin.js', array( 'jquery' ), $this->version, false );

	}

}

<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://christophrado.de
 * @since      1.0.0
 *
 * @package    Woocommerce_Wertewandel
 * @subpackage Woocommerce_Wertewandel/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

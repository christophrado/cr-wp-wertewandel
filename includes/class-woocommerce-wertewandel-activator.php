<?php

/**
 * Fired during plugin activation
 *
 * @link       https://christophrado.de
 * @since      1.0.0
 *
 * @package    Woocommerce_Wertewandel
 * @subpackage Woocommerce_Wertewandel/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Woocommerce_Wertewandel
 * @subpackage Woocommerce_Wertewandel/includes
 * @author     Christoph Rado <wordpress@christophrado.de>
 */
class Woocommerce_Wertewandel_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}

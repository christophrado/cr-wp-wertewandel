<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://christophrado.de
 * @since      1.0.0
 *
 * @package    Woocommerce_Wertewandel
 * @subpackage Woocommerce_Wertewandel/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Woocommerce_Wertewandel
 * @subpackage Woocommerce_Wertewandel/public
 * @author     Christoph Rado <wordpress@christophrado.de>
 */
class Woocommerce_Wertewandel_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		if( 'no' == get_option( 'wcww_settings_use_native_input' ) ) {

			self::wertewandel_enqueue_api_script();

			add_action( 'woocommerce_checkout_before_customer_details', __CLASS__ . '::wertewandel_checkout_banner' );

		} else {

			add_action( 'woocommerce_after_order_notes', __CLASS__ . '::wertewandel_account_number_field' );
	
			add_action( 'woocommerce_checkout_process', __CLASS__ . '::wertewandel_account_number_verification' );

		}

		add_action( 'woocommerce_checkout_before_customer_details', __CLASS__ . '::wertewandel_test_json' );

		add_action( 'woocommerce_checkout_update_order_meta', __CLASS__ . '::wertewandel_account_number_update_order_meta' );

		add_action( 'woocommerce_admin_order_data_after_billing_address', __CLASS__ . '::wertewandel_customer_number_display_admin_order_meta', 10, 1 );

	}

	/**
	 * Add the field to the checkout
	 */
	public static function wertewandel_account_number_field( $checkout ) {
	
		echo '<div id="wertewandel_account"><h3>' . __( 'Wertewandel', 'woocommerce-wertewandel' ) . '</h3>';

		if( is_user_logged_in() )
			$wertewandel_customer_number = get_user_meta( get_current_user_id(), 'wertewandel-customer-no', true );
		if( empty( $wertewandel_customer_number) )
			$wertewandel_customer_number = $checkout->get_value( 'wertewandel-customer-no' );

		woocommerce_form_field( 'wertewandel-customer-no', array(
			'type'          => 'text',
			'class'         => array( 'form-row-wide' ),
			'label'         => __( 'Wertewandel Customer Number', 'woocommerce-wertewandel' ),
		), $wertewandel_customer_number );

		if( 'yes' == get_option( 'wcww_settings_show_registration_info' ) && empty( $wertewandel_customer_number ) )
			echo '<small>' . sprintf( __( 'Not a Wertewandel user yet? <a href="%s" target="_blank">Register now!</a>', 'woocommerce-wertewandel' ), 'https://wertewandel.de/registrieren/' ) . '</small>';
		echo '</div>';
	
	}

	/**
	 * Process the checkout
	 */
	public static function wertewandel_account_number_verification() {

		if ( ! $_POST['wertewandel-customer-no'] )
			return false;

		$request = wp_remote_get( API_HOST . 'customers/check?number=' . $_POST['wertewandel-customer-no'] );

		if( is_wp_error( $request ) )
			return false;

		$responseCode = wp_remote_retrieve_response_code( $request );

		if( '200' != $responseCode )
			wc_add_notice( __( "Your customer number could not be confirmed with Wertewandel. Please verify your input.", 'woocommerce-wertewandel'), 'error' );

	}

	/**
	 * Update the order meta with field value
	 */
	public static function wertewandel_account_number_update_order_meta( $order_id ) {

		if ( ! empty( $_POST['wertewandel-customer-no'] ) ) {

			update_post_meta( $order_id, 'wertewandel-customer-no', sanitize_text_field( $_POST['wertewandel-customer-no'] ) );

			if( is_user_logged_in() )
				update_user_meta( get_current_user_id(), 'wertewandel-customer-no', sanitize_text_field( $_POST['wertewandel-customer-no'] ) );

		}

	}

	/**
	 * Display field value on the order edit page
	 */
	public static function wertewandel_customer_number_display_admin_order_meta($order) {
		echo '<p><strong>' . __( 'Wertewandel Customer Number', 'woocommerce-wertewandel' ) . ':</strong> ' . get_post_meta( $order->id, 'wertewandel-customer-no', true ) . '</p>';
	}


	public static function wertewandel_enqueue_api_script() {
		wp_enqueue_script( 'wertewandel-partner-checkout', 'https://assets.wertewandel.de/partners/v1/checkout.js' );
	}

	public static function wertewandel_checkout_banner() {
		echo '<div id="wertewandel-checkout" data-partnerid="' . get_option( 'wcww_settings_partnerid' ) . '" data-sum="' . WC()->cart->total . '"></div>';
	}



	public static function get_access_token() {

		$request = wp_remote_post( API_HOST . API_VERSION . '/auth/apps/' . get_option( 'wcww_settings_appid' ), array(
			'headers' => array(
				'Content-Type' => 'application/json; charset=utf-8'
			),
			'body' => json_encode( array(
				"auth_token" => get_option( 'wcww_settings_authtoken' ),
			)),
			'method' => 'POST'
		));

		if( is_wp_error( $request ) )
			return false;

		if( '200' != wp_remote_retrieve_response_code( $request ) )
			return 'Error. Response Code: ' . wp_remote_retrieve_response_code( $request );

		$response = wp_remote_retrieve_body( $request );
		$array = json_decode( $response );
		return $array->access_token;

	}


	public static function create_receipt_json ( $order ) {
		$receiptArray = array(
			"customer_number"		=> $order->get_meta( 'wertewandel-customer-no' ),
			"partner_id"			=> get_option( 'wcww_settings_partnerid' ),
			"time"					=> $order->get_date_created()->date( 'Y-m-d' ) . 'T' . $order->get_date_created()->date( 'H:m:s' ) . 'Z',
			"total_price"			=> (float) $order->get_total(),
		);
		foreach( $order->get_items() as $item ){
			$receiptArray['items'][] = array(
				"txt"				=> $item->get_name(),
				"product_reference"	=> $item->get_product()->get_sku(),
				"price_per_item"	=> (float) $item->get_product()->get_price(),
				"amount"			=> $item->get_quantity(),
			);
		}
		if( $fees = $order->get_fees() ) {
			$fee_total = 0;
			foreach( $fees as $fee ){
				$fee_total += $fee->get_total();
			}
			$receiptArray['items'][] = array(
				"txt"				=> "Gebühr",
				"product_reference"	=> "fee",
				"price_per_item"	=> (float) $fee_total,
				"amount"			=> 1,
				"discountable"		=> false,
			);
		}
		if( $order->get_discount_total() ) {
			$receiptArray['items'][] = array(
				"txt"				=> "Gutschein",
				"product_reference" => "discount",
				"price_per_item"	=> - (float) $order->get_discount_total(),
				"amount"			=> 1,
				"discountable"		=> false,
			);
		}
		if( $order->get_shipping_total() ) {
			$receiptArray['items'][] = array(
				"txt"				=> "Versand",
				"product_reference" => "shipping",
				"price_per_item"	=> (float) $order->get_shipping_total(),
				"amount"			=> 1,
				"discountable"		=> false,
			);
		}
		return json_encode( $receiptArray );
	}


	public static function send_receipt_to_api ( $receipt, $accessToken ) {

		$request = wp_remote_post( API_HOST . API_VERSION . '/receipts/', array(
			'headers' => array(
				'Content-Type' => 'application/json; charset=utf-8',
				'Authorization' => 'Bearer ' . $accessToken,
			),
			'body' => $receipt,
			'method' => 'POST'
		));

		if( is_wp_error( $request ) )
			return false;

		if( '201' != wp_remote_retrieve_response_code( $request ) )
			return 'Error. Response Code: ' . wp_remote_retrieve_response_code( $request );

		$response = wp_remote_retrieve_body( $request );

		// DEBUG
/*
		$responseCode = wp_remote_retrieve_response_code( $request );
		$array = json_decode( $response );
		print "<pre>";
		print_r( $responseCode . '<br/>' );
		print_r( 'ID: ' . $array->receipt->id . '<br/>' );
		print_r( $response );
		print "</pre>";
*/

		return json_decode( $response )->receipt->id;

	}



	public static function wertewandel_test_json() {

		echo 'Test Order ID: 164<br/>';
		$order = new WC_Order( 164 );

		$receipt = self::create_receipt_json( $order );

		// DEBUG
		print "<pre>";
// 		print_r( $receipt );
		print_r( json_decode( $receipt ) );
		print "</pre>";

		$accessToken = self::get_access_token();

		// DEBUG
/*
		print "<pre>";
		print_r( 'access_token: ' . $accessToken );
		print "</pre>";
*/

		$receiptID = self::send_receipt_to_api ( $receipt, $accessToken );

		print "<pre>";
		print_r( 'receipt id: ' . $receiptID );
		print "</pre>";

	}


	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Woocommerce_Wertewandel_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Woocommerce_Wertewandel_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/woocommerce-wertewandel-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Woocommerce_Wertewandel_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Woocommerce_Wertewandel_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/woocommerce-wertewandel-public.js', array( 'jquery' ), $this->version, false );

	}

}
